import { Global,Module,HttpException } from '@nestjs/common';
import { AppController } from './app.controller';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { PassportModule } from '@nestjs/passport';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from './users/users.module';
import { UtilService } from './util/util.service';
import { JwtStrategy } from './JWT/jwt.strategy';
import { RavenInterceptor } from 'nest-raven';
import * as dotenv from 'dotenv';
dotenv.config();
@Global()
@Module({
  imports: [ MongooseModule.forRootAsync({
    useFactory: () => ({
      uri: (process.env.NODE_ENV == 'production') ? process.env.MONGO_DB_URL_PRODUCTION : process.env.MONGO_DB_URL_STAGING,
      useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true,
      useCreateIndex: true
    }),
  }),
  PassportModule.register({ defaultStrategy: 'jwt' }),
  JwtModule.register({ secret: process.env.SECRET, signOptions: { expiresIn: '3h' } }),
  UsersModule,
 ],
  controllers: [AppController],
  providers: [UtilService,JwtStrategy,
    {
      provide: APP_INTERCEPTOR,

      useValue: new RavenInterceptor({
        filters: [
          // Filter exceptions of type HttpException. Ignore those that
          // have status code of less than 500
          { type: HttpException, filter: (exception: HttpException) => 500 > exception.getStatus() },
        ],
      }),
    },],
  exports:[MongooseModule, JwtModule, UsersModule, UtilService,JwtStrategy,]
})

export class AppModule {}
