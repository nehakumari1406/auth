import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import{UsersModule} from './users/users.module';
import * as mongoose from 'mongoose';
import * as dotenv from 'dotenv';
import * as Sentry from '@sentry/node';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
async function bootstrap() {
  dotenv.config();
  const app = await NestFactory.create(AppModule);
  const { env } = process;
  if (env.NODE_ENV === "dev") {
    mongoose.set('debug', true)
  }
  
  
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
    next();
  });
  if (process.env.NODE_ENV === 'production') Sentry.init({ dsn: "https://9c84670941d7445db564e262769d1916@o1091520.ingest.sentry.io/6110200", tracesSampleRate: 1.0, });

  
  if (process.env.PREDIFINED && process.env.PREDIFINED == "true") {
    let options = new DocumentBuilder().setTitle('user').setBasePath("/").setVersion('v1').addBearerAuth().setSchemes('https', 'http').build();

    const document = SwaggerModule.createDocument(app, options, {
      include: [UsersModule]
    });
    SwaggerModule.setup('/explorer', app, document);
  }
  const port = process.env.PORT || 3000;
  await app.listen(port);
  console.log(`http://localhost:${port}/explorer/#/`)
}
bootstrap();
