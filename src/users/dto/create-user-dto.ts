import { Type } from 'class-transformer';
import {
    IsNotEmpty,
    Length,
    IsString,
   
} from 'class-validator';
import { ApiModelProperty} from '@nestjs/swagger';

export class company {
    name: string
    catchPhrase: string
    bs: string
}
export class CompanyDTO {
    @ApiModelProperty()
    @IsString()
    name: string;

    @ApiModelProperty()
    @IsString()
    catchPhrase: string;

    @ApiModelProperty()
    @IsString()
    bs: string;


}
export class CompanyUpdateDTO {
    @ApiModelProperty()
    company: CompanyDTO

}

export class geo {
    lat: string
    lng: string

}
export class geoDTO {
    @ApiModelProperty()
    @IsString()
    lat: string;

    @ApiModelProperty()
    @IsString()
    lng: string;

}
export class GeoUpdateDTO {
    @ApiModelProperty()
    geo: geoDTO

}
export class address {
    street: string
    suite: string
    city: string
    zipcode: string

}
export class AddressDTO {
    @ApiModelProperty()
    @IsString()
    street: string;

    @ApiModelProperty()
    @IsString()
    suite: string;

    @ApiModelProperty()
    @IsString()
    city: string;

    @ApiModelProperty()
    @IsString()
    zipcode: string;

    @ApiModelProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;


}
export class AddressUpdateDTO {
    @ApiModelProperty()
    address: AddressDTO;



}
export class CreateuserDto {
    @ApiModelProperty()
    @IsNotEmpty()
    username: string;

    @ApiModelProperty()
    @IsNotEmpty()
    name: string;

    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    @Length(6, 35)
    password: string;
    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    @Length(10, 11)
    phone: string;

    @ApiModelProperty()
    @IsNotEmpty()
    email: string;
    @ApiModelProperty()
    @IsNotEmpty()
    website: string;


    role: string;
    deliveryManagementBy: string;

    @ApiModelProperty({ type: AddressDTO })
    @Type(() => AddressDTO)
    address: AddressDTO;
    @ApiModelProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;

    @ApiModelProperty({ type: CompanyDTO })
    @Type(() => CompanyDTO)
    company: CompanyDTO;
}