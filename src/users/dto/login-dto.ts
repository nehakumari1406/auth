import {
    IsNotEmpty,
    Length,
    IsString,
   
} from 'class-validator';
import { ApiModelProperty} from '@nestjs/swagger';

export class LoginDTO {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
     email: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @Length(6, 35)
    @IsString()
    password: string;


}