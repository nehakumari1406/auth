
import {
    IsNotEmpty,
    Length,
    IsString,IsOptional,IsEmpty,IsEnum
   
} from 'class-validator';
import { UserRoles } from '../../util/app.model';
import { Type } from 'class-transformer';
import { ApiModelProperty} from '@nestjs/swagger';


export class company {
    name: string
    catchPhrase: string
    bs: string
}
export class CompanyDTO {
    @ApiModelProperty()
    @IsString()
    name: string;

    @ApiModelProperty()
    @IsString()
    catchPhrase: string;

    @ApiModelProperty()
    @IsString()
    bs: string;


}
export class CompanyUpdateDTO {
    @ApiModelProperty()
    company: CompanyDTO

}

export class geo {
    lat: string
    lng: string

}
export class geoDTO {
    @ApiModelProperty()
    @IsString()
    lat: string;

    @ApiModelProperty()
    @IsString()
    lng: string;

}
export class GeoUpdateDTO {
    @ApiModelProperty()
    geo: geoDTO

}

export class address {
    street: string
    suite: string
    city: string
    zipcode: string

}
export class AddressDTO {
    @ApiModelProperty()
    @IsString()
    street: string;

    @ApiModelProperty()
    @IsString()
    suite: string;

    @ApiModelProperty()
    @IsString()
    city: string;

    @ApiModelProperty()
    @IsString()
    zipcode: string;

    @ApiModelProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;


}
export class AddressUpdateDTO {
    @ApiModelProperty()
    address: AddressDTO;



}
export class UsersDTO {
    @IsEmpty()
    _id: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    username: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    email: string;

    @IsString()
    @IsNotEmpty()
    @Length(5, 35)
    @ApiModelProperty()
    password: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    phone: string;

    @IsString()
    @ApiModelProperty()
    website: string;
    
    @IsNotEmpty()
    @ApiModelProperty({ enum: Object.keys(UserRoles) })
    @IsEnum(UserRoles, {
        message: 'Role type must be one of these ' + Object.keys(UserRoles),
    })
    role: string;

    @ApiModelProperty({ type: AddressDTO })
    @Type(() => AddressDTO)
    address: AddressDTO;
    @ApiModelProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;

    @ApiModelProperty({ type: CompanyDTO })
    @Type(() => CompanyDTO)
    company: CompanyDTO;
    status: boolean;


}