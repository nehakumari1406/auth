import * as mongoose from 'mongoose';
export const UserSchema = new mongoose.Schema(
    {
        name: { type: String },
        username: { type: String },
        email: { type: String, trim: true, lowercase: true, sparse: true },
        role: { type: String },
        password: { type: String },
        status: { type: Boolean, default: true },
        address: {
            street: { type: String },
            suite: { type: String },
            city: { type: String },
            zipcode: { type: String },

            geo: {
                lat: { type: String },
                lng: { type: String }
            }
        },
        phone: { type: String },
        website: { type: String },
        company: {
            name: { type: String },

            catchPhrase: { type: String },
            bs: { type: String }
        }


    },

);