import { Controller,Post,Body,Get ,UseGuards} from '@nestjs/common';
import{LoginDTO} from './dto/login-dto';
import{ApiUseTags,ApiBearerAuth} from '@nestjs/swagger';
import { UsersService } from './users.service';
import{AuthService} from '../auth/auth.service';
import { AuthGuard } from '@nestjs/passport';
import { UtilService } from 'src/util/util.service';
import { GetUser } from '../jwt/jwt.strategy';
import { UsersDTO } from './dto/user-dto';

import { CreateuserDto } from './dto/create-user-dto';
import{UserRoles}from'../util/app.model';



@Controller('users')
@ApiUseTags('Users')
export class UsersController {
    constructor(
        private userService: UsersService,
        private authService:AuthService,
        private utilservice:UtilService
    ){

    }

    /* ################################################### LOGIN ################################## */

    @Post('admin/login')
      public async user(@Body() data: LoginDTO) {
        const user= await this.userService.getemail(data.email);
         const token = await this.authService.accesstoken(user._id, user.role);
        return this.utilservice.successResponseData({ token: token, id: user._id, role: user.role });
        
    }
    @Get('admin/getallusers')
    public async getall() {
            const user = await Promise.all([
                this.userService.userList(),
                this.userService.countuserList()
            ]);
            return this.utilservice.successResponseData(user[0],{total:user[1]});
      
    }
    @Post('admin/create')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
  
    public async create( @GetUser() user: UsersDTO, @Body() createuserData: CreateuserDto) {
      console.log(user);
      if (user.role === UserRoles.ADMIN) {
       createuserData.role = UserRoles.USER;
        const users = await this.userService.createusers(createuserData);
         console.log(users)
         if (users) return this.utilservice.successResponseData('USER_SAVED');
           
      }

    }

   
}
