import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersService } from './users.service';
import { UserSchema } from './dto/userschema';
import{AuthService} from '../auth/auth.service';


@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),

],
  controllers: [UsersController],
  providers: [UsersService,AuthService],
  exports: [UsersService, MongooseModule]
})
export class UsersModule {}
