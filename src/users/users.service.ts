import { Injectable } from '@nestjs/common';
import { UsersDTO } from './dto/user-dto';
import{CreateuserDto} from './dto/create-user-dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class UsersService {
    constructor(
   
    @InjectModel('User') private readonly userModel: Model<any>,
    ){}
    public async createusers(createuserData: CreateuserDto): Promise<any> {
        const users = await this.userModel.create(createuserData);
        return users;
    }
    public async getemail(email: String):Promise<UsersDTO> {
        const user = await this.userModel.findOne({ email: email });
        
        return user;
    }
    public async userList(): Promise<Array<any>> {
        let filter = {};
        let user = await this.userModel.find(filter);
        return user;
    }
   
    public async countuserList(): Promise<number> {
        let filter = {};
        return await this.userModel.countDocuments(filter);
    }
    public async getUserDeatilForJWTValidate(userId: string): Promise<UsersDTO> {
        const user = await this.userModel.findById(userId, 'name username email phone status website role');
        return user;
    }
}
