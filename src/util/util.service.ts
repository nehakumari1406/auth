import { Injectable,HttpStatus,UnauthorizedException, } from '@nestjs/common';


let languageList = [];
let language="en";
@Injectable()
export class UtilService {


    public async successResponseData(responseData, extra?) {
        if (!extra) return await this.res(HttpStatus.OK, responseData);
        let res = await this.res(HttpStatus.OK, responseData);
        for (var key in extra) res[key] = extra[key];
        return res;
    }
    public unauthorized() {
        const msg = this.getTranslatedMessage('UNAUTHORIZED');
        throw new UnauthorizedException(msg);
    }

    private async res(responseCode, responseData?, key?) {
        let message = "";
        if (responseData) {
            message = responseData;
        } else {
            if (languageList && languageList[language] && languageList[language][key]) {
                message = languageList[language][key];
            } else {
                message = languageList["en"][key];
            }
        }
        message = message || key;
        return {
            response_code: responseCode,
            response_data: message
        }
    }
    
    
    public getTranslatedMessage(key) {
        let message = "";
        if (languageList && languageList[language] && languageList[language][key]) {
            message = languageList[language][key];
        } else {
            message = languageList["en"][key];
        }
        return message ? message : key;
    }
    
}
